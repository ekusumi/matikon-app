//
//  MapWebViewController.h
//  Emerald
//
//  Created by Razvan on 7/3/13.
//  Copyright (c) 2013 coltboy. All rights reserved.
//

#import <UIKit/UIKit.h>

//=======================================================================
// MapWebViewController - Public Interface
//=======================================================================
@interface MapWebViewController : UIViewController <UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) NSDictionary *articleContent;

@end
