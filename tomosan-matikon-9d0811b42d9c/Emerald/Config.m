//
//  Config.m
//  Emerald
//
//  Created by ColtBoys on 12/21/12.
//  Copyright (c) 2012 coltboy. All rights reserved.
//

#import "Config.h"
#import "Tools.h"

// DO NOT EDIT
#define FeedCellSize 240;
#define PhotoCellSize 240;
#define VideoCellSize 240;
// END DO NOT EDIT




//Controllers

#define TabControllers @"feed/Top,social/Social,more/Myself,game/Game"


// Style
#define MainFont @"パズルアイドル"
#define MainFontSize 20
#define MainColor @"#65ac9e"
//Feed
#define FeedSharingMessage @"You can change this text : "
#define FeedUrl @"http://iphone.lparty.jp/events/xml"
#define FeedSearchUrl @"http://iphone.lparty.jp/events/xml"
//@"http://iphone.lparty.jp/events/search"
#define FeedTitleColor @"#000000"
#define FeedArticleFontSize 14
#define FeedTitleFontSize 14
#define FavouritesEnabled @"YES"
#define FeedRibbonEnabled @"YES"
#define FeedArticleTextColor @"#000000"
//Photo
#define PhotoSharingMessage @"You can change this text : "
#define PhotoUrl @"http://coltboy.com/products/Emerald/photos.xml"
#define PhotoTitleColor @"#000000"

#define PhotoRibbonEnabled @"YES"
//Video
#define VideoSharingMessage @"You can change this text : "
#define VideoUrl @"http://182.48.52.97/a0/index.xml"
#define VideoTitleColor @"#000000"

#define VideoRibbonEnabled @"YES"
//Audio
#define AudioUrl @"http://coltboy.com/products/Emerald/audio.xml"
#define AudioFontSize 19
#define AudioTextColor @"#ffffff"
#define AudioRibbonEnabled @"YES"
#define AudioColor @"#22967c"
#define AudioSharingMessage @"You can change this text : "
//More
#define MoreCells @"favorites#お気に入り;url#http://lparty.jp/company#運営会社;text#\n1．著作権当ウェブサイトに記載されている内容の著作権は、NRIグループ及びコンテンツの作者に帰属します。当ウェブサイト及びそのコンテンツを使用できるのは、著作権法上「私的使用のための複製」及び「引用」などの場合に限られています。この場合、出典を明記してください。なお、「私的使用のための複製」や「引用」の範囲を超える場合には、NRIの使用許諾が必要になります。使用許諾のお申し込みやお問い合わせは、NRIコーポレートコミュニケーション部（Tel.03-6270-8100／information@nri.co.jp）にご連絡ください。\n\n2．リンク当ウェブサイトへのリンクは自由です。ただし、違法または公序良俗に反するとNRIが考えるサイトからのリンクや、当ウェブサイトへのリンクであるということが不明確になる手段でのリンクについてはお断りすることがあります。トップページ以外にリンクされた場合には、そのページのコンテンツやURLが、予告なしに変更又は廃止される可能性があることをご了解ください。\n\n3．準拠法および管轄裁判所当ウェブサイトおよび「サイト利用規定」の解釈および適用は、日本国法に準拠します。また、当ウェブサイトに関わる全ての紛争については、他に別段の定めのない限り、東京地方裁判所を第一審の専属管轄裁判所とします。\n\n4．対応ブラウザ当ウェブサイトを閲覧する際には、Microsoft Internet Explorer 6.0以上、Firefox 3.0以上のブラウザの使用を推奨します。これら推奨ブラウザ以外でご覧いただく場合、画面の一部が正しく表示されないことがありますので、ご了解ください。\n\n5．プラグイン当ウェブサイトに収録されているPDFファイルをご覧いただくには、Adobe Readerが必要です。当ウェブサイトに収録されている一部コンテンツはFlashが使われております。これらのコンテンツをご覧いただくためには、Adobe Flashが必要です。この他、コンテンツによっては別途プラグインが必要となる場合もあります。各コンテンツのプラグイン情報等をご参照ください。\n\n6．お問い合わせ先当ウェブサイト・当社についてご意見・ご質問などがある場合は、「お問い合わせ」ページをご確認の上、各担当までメールにてご連絡下さい。お問い合わせ先が不明の場合は、NRIコーポレートコミュニケーション部（Tel.03-6270-8100）にご連絡ください。\n\n\n\n\n\n#利用規定;url#http://lparty.jp/contract#お問い合わせ"
#define MoreFontSize 19
#define MoreTextColor @"#000000"
//Strings
#define StringLoading @"読込中"
#define StringPublishOn @"公開中"
#define StringRemoveFromList @"お気に入りから外す"
#define StringReadItLater @"お気に入り登録"
#define StringShare @"シェアする"
#define StringCancel @"キャンセル"
#define StringOK @"OK"
#define StringTwitterAccountMissing @"Twitterのアカウントがありません。"
#define StringTwitterAccountMissingMessage @"Twitterのアカウントが設定されていません。設定からアカウントを登録してください。"
#define StringFacebookAppMissing @"Facebook App missing"
#define StringFacebookAppMissingMessage @"Please download the official facebook application to share this content."
#define StringError @"Error"
#define StringErrorMessage @"We are unable to refresh the content at this time. Please try again in a moment"
#define StringNetworkError @"No network connection"
#define StringNetworkErrorMessage @"It appears there is no avalaible network connection. Try to find a WiFi point or look at your device settings"
#define StringAudioStreamingError @"We can not load this song at the moment, please try again later"
//
@implementation Config
+(NSString *)getTabControllers{
    return TabControllers;
}

//Style
+(UIColor *)getMainColor{
    return [Tools colorWithHexString:MainColor];
}
+(UIFont *)getMainFont{
    return [UIFont fontWithName:MainFont size:MainFontSize];
}
//Feed
+(NSString *)getFeedSharingMessage{
    return FeedSharingMessage;
}
+(NSString *)getFeedUrl{
    return FeedUrl;
}
+(NSString *)getFeedSearchUrl{
    return FeedSearchUrl;
}

+(NSString *)getFeedFontString{
    return MainFont;
}
+(float)getFeedArticleSize{
    return FeedArticleFontSize;
}
+(UIFont *)getFeedFont{
    return [UIFont fontWithName:MainFont size:FeedTitleFontSize];
}
+(UIColor *)getFeedTitleColor{
    return [Tools colorWithHexString:FeedTitleColor];
}
+(CGFloat)getFeedCellSize{
    return FeedCellSize;
}
+(BOOL)isFavouritesEnabled{
    if ([FavouritesEnabled isEqualToString:@"YES"]) {
        return YES;
    }
    else
    {
        return NO;
    }
}
+(BOOL)isFeedRibbonEnabled{
    if ([FeedRibbonEnabled isEqualToString:@"YES"]) {
        return YES;
    }
    else
    {
        return NO;
    }
}
+(UIColor *)getFeedArticleTextColor{
    return [Tools colorWithHexString:FeedArticleTextColor];
}
+(NSString *)getFeedArticleTextColorString{
    return FeedArticleTextColor;
}
//Photo
+(NSString *)getPhotoSharingMessage{
    return PhotoSharingMessage;
}
+(NSString *)getPhotoUrl{
    return PhotoUrl;
}
+(UIColor *)getPhotoTitleColor{
    return [Tools colorWithHexString:PhotoTitleColor];
}
+(CGFloat)getPhotoCellSize{
    return PhotoCellSize;
}
+(BOOL)isPhotoRibbonEnabled{
    if ([PhotoRibbonEnabled isEqualToString:@"YES"]) {
        return YES;
    }
    else
    {
        return NO;
    }
}
//Video
+(NSString *)getVideoSharingMessage{
    return VideoSharingMessage;
}
+(NSString *)getVideoUrl{
    return VideoUrl;
}
+(UIColor *)getVideoTitleColor{
    return [Tools colorWithHexString:PhotoTitleColor];
}
+(CGFloat)getVideoCellSize{
    return VideoCellSize;
}
+(BOOL)isVideoRibbonEnabled{
    if ([VideoRibbonEnabled isEqualToString:@"YES"]) {
        return YES;
    }
    else
    {
        return NO;
    }
}
//Audio
+(NSString *)getAudioUrl{
    return AudioUrl;
}
+(UIFont *)getAudioFont{
    return [UIFont fontWithName:MainFont size:AudioFontSize];
}
+(UIColor *)getAudioTextColor{
    return [Tools colorWithHexString:AudioTextColor];
}
+(BOOL)getAudioRibbonEnabled{
    if ([AudioRibbonEnabled isEqualToString:@"YES"]) {
        return YES;
    }
    else
    {
        return NO;
    }
}
+(UIColor *)getAudioColor{
    return [Tools colorWithHexString:AudioColor];
}
+(NSString *)getAudioSharingMessage{
    return AudioSharingMessage;
}
//More
+(NSString *)getMoreCellString{
    return MoreCells;
}
+(UIFont *)getMoreFont{
    return [UIFont fontWithName:MainFont size:MoreFontSize];
}
+(UIColor *)getMoreTextColor{
    return [Tools colorWithHexString:MoreTextColor];
}
//Strings
+(NSString *)getStringPublishedOn{
    return StringPublishOn;
}
+(NSString *)getStringRemoveFromList{
    return StringRemoveFromList;
}
+(NSString *)getStringReadItLater{
    return StringReadItLater;
}
+(NSString *)getStringShare{
    return StringShare;
}
+(NSString *)getStringCancel{
    return StringCancel;
}
+(NSString *)getStringOK{
    return StringOK;
}
+(NSString *)getStringTwitterAccountMissing{
    return StringTwitterAccountMissing;
}
+(NSString *)getStringTwitterAccountMissingMessage{
    return StringTwitterAccountMissingMessage;
}
+(NSString *)getStringFacebookAppMissing{
    return StringFacebookAppMissing;
}
+(NSString *)getStringFacebookAppMissingMessage{
    return StringFacebookAppMissingMessage;
}
+(NSString *)getStringError{
    return StringError;
}
+(NSString *)getStringErrorMessage{
    return StringErrorMessage;
}
+(NSString *)getStringNetworkError{
    return StringNetworkError;
}
+(NSString *)getStringNetworkErrorMessage{
    return StringNetworkErrorMessage;
}
+(NSString *)getStringAudioError{
    return StringAudioStreamingError;
}
@end
